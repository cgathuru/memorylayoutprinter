/* 
 * File:   main.c
 * Author: Charles Gathuru
 * Student no: 260457189
 *
 * Created on January 29, 2014, 11:30 PM
 */

#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/resource.h>
#include <string.h>

extern char __executable_start, etext, edata, end;


/*
 * 
 */
int main(int argc, char* argv[]) {

    int i=0;
    /* Print out the start and */
    if(strcmp("text",argv[1]) == 0)
    {
        printf("The start of the text segment is :     %10p\n",&__executable_start);
        printf("The end of the text segment is   :     %10p\n",&etext);
    }
    else if (strcmp("data",argv[1]) == 0)
    {
        printf("The start of the data segment is :     %10p\n",&etext);
        printf("The end of the data segment is   :     %10p\n",&edata);
    }
    else if (strcmp("bss",argv[1]) == 0)
    {
        printf("The start of the BSS segment is  :     %10p\n",&edata);
        printf("The end of the BSS segment is    :     %10p\n",&end);
    }
    else if (strcmp("stack",argv[1]) == 0)
    {
        /* To calculate the size of the stack segment uncomment this section */
        printf("The start of the stack segment is:     %10p\n", &i);
        struct rlimit limit;
        getrlimit(RLIMIT_STACK,&limit); /* Gets the maximum limit of the stack */
        printf("The size of the stack segment is  :     %ld\n",limit.rlim_cur);
    }
    else if(strcmp("heap",argv[1]) == 0)
    {
        char *ptr= malloc(1024*1024);
        printf("Beginning of Heap            %10p\n", ptr);
        char *ptr_2 = malloc(1024*1024*1024);
        printf("Allocating 1GB, starting address:   %10p\n",ptr_2);
        char *ptr_3 = malloc(1024*1024*1024);
        printf("Allocating another GB, starting address: %10p\n",ptr_3);
        printf("Commencing memory allocation KB per KB.........\n");
        while(1) {
                ptr = malloc(1024);
                if(ptr == NULL)
                        break;
                printf("End of Heap                  %10p\n", ptr + sizeof(ptr));
        }
    }
    else if(strcmp("mmap",argv[1]) == 0)
    {
        int fd = open("memory_layout_printer.c", O_RDONLY);
        void *ptr_mmap = (void *) mmap(NULL, 1024, PROT_WRITE, MAP_PRIVATE| MAP_ANONYMOUS,-1,0);
        printf("The start of the memory mapped segment is:  %10p\n",ptr_mmap);
        void *ptr_mmap_2 = (void *) mmap(NULL, 1024*1024*1024, PROT_WRITE, MAP_PRIVATE| MAP_ANONYMOUS,-1,0);
        printf("Allocating 1GB, starting address:   %10p\n",ptr_mmap_2);
        printf("Commencing memory allocation KB per KB.........\n");

        // Do a memory map of the file
        while(1)
        {
            ptr_mmap_2 = (void *) mmap(NULL, 1024, PROT_WRITE, MAP_PRIVATE| MAP_ANONYMOUS,-1,0);
            if(strcmp("0xffffffff",ptr_mmap_2) == 0)
               {
                    printf("BreaK");
                    break;
               }
            printf("End of mmap segment is:	%10p\n",ptr_mmap_2);
            printf("Current size is: %10p\n", (void *)(ptr_mmap - ptr_mmap_2));      
        }
    }
    else{}
    
    return (EXIT_SUCCESS);
}

